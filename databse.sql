-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.20 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table users_database.issues: ~0 rows (approximately)
/*!40000 ALTER TABLE `issues` DISABLE KEYS */;
INSERT INTO `issues` (`issue_key`, `assignee`, `end_date`, `issue_id`, `original_estimate`, `parent_id`, `sprint`, `start_date`, `status`, `time_spent`) VALUES
	('0', '0', NULL, 0, 0, 0, 'FT 3 Sprint 01', NULL, 0, 0),
	('1', '0', NULL, 0, 0, 0, 'FT 4 Sprint 01', NULL, 0, 0),
	('2', '0', NULL, 0, 0, 0, 'FT 5 Sprint 01', NULL, 0, 0),
	('3', '0', NULL, 0, 0, 0, 'FT 6 Sprint 01', NULL, 0, 0),
	('4', '0', NULL, 0, 0, 0, 'FT 2 Sprint 01', NULL, 0, 0),
	('5', '0', NULL, 0, 0, 0, 'FT 2 Sprint 02', NULL, 0, 0),
	('PS-4317', 'nv.diep', NULL, 37639, 0, 0, 'FT 2 Sprint 53', NULL, 10001, 0),
	('PS-4407', 'kimanh', NULL, 38111, 0, 0, 'FT 2 Sprint 53', NULL, 10001, 0),
	('PS-4417', 'vinh.nguyenngoc', NULL, 38165, 3, 37041, 'FT 2 Sprint 53', '2020-04-13 00:00:00', 10011, 0),
	('PS-4418', 'tuyet.trinhthianh', NULL, 38166, 3, 36142, 'FT 2 Sprint 53', '2020-04-13 00:00:00', 10011, 3),
	('PS-4419', 'phuong.vulan', NULL, 38167, 0.75, 38108, 'FT 2 Sprint 53', '2020-04-13 00:00:00', 10006, 0),
	('PS-4420', 'phuong.vulan', NULL, 38169, 3, 37041, 'FT 2 Sprint 53', '2020-04-13 00:00:00', 10011, 3),
	('PS-4421', 'vinh.nguyenngoc', NULL, 38171, 0.375, 38108, 'FT 2 Sprint 53', '2020-04-13 00:00:00', 10011, 0),
	('PS-4422', 'kimanh', NULL, 38173, 3, 37289, 'FT 2 Sprint 53', '2020-04-13 00:00:00', 10006, 3),
	('PS-4430', 'kimanh', NULL, 38219, 3, 37289, 'FT 2 Sprint 53', '2020-04-14 00:00:00', 10006, 3),
	('PS-4431', 'tuyet.trinhthianh', NULL, 38220, 3, 36142, 'FT 2 Sprint 53', '2020-04-14 00:00:00', 10011, 3),
	('PS-4432', 'nv.diep', NULL, 38221, 3, 36993, 'FT 2 Sprint 53', '2020-04-14 00:00:00', 10006, 3),
	('PS-4433', 'vinh.nguyenngoc', NULL, 38222, 0.9375, 37041, 'FT 2 Sprint 53', '2020-04-14 00:00:00', 10011, 0),
	('PS-4434', 'vinh.nguyenngoc', NULL, 38223, 2.0625, 37041, 'FT 2 Sprint 53', '2020-04-14 00:00:00', 10011, 0),
	('PS-4442', 'phuong.vulan', NULL, 38306, 3, 36142, 'FT 2 Sprint 53', '2020-04-15 00:00:00', 10011, 3),
	('PS-4446', 'phuong.vulan', NULL, 38346, 1.5, 37639, 'FT 2 Sprint 53', '2020-04-14 00:00:00', 10011, 1),
	('PS-4449', 'tuyet.trinhthianh', NULL, 38351, 3, 36142, 'FT 2 Sprint 53', '2020-04-15 00:00:00', 10011, 3),
	('PS-4450', 'nv.diep', NULL, 38359, 2.25, 36993, 'FT 2 Sprint 53', '2020-04-15 00:00:00', 10006, 2),
	('PS-4454', 'vinh.nguyenngoc', NULL, 38371, 3, 37041, 'FT 2 Sprint 53', '2020-04-15 00:00:00', 10011, 0),
	('PS-4455', 'kimanh', NULL, 38375, 3, 37289, 'FT 2 Sprint 53', '2020-04-15 00:00:00', 10006, 3),
	('PS-4467', 'kimanh', NULL, 38417, 3, 37289, 'FT 2 Sprint 53', '2020-04-16 00:00:00', 10006, 3),
	('PS-4468', 'nv.diep', NULL, 38418, 3, 36993, 'FT 2 Sprint 53', '2020-04-16 00:00:00', 10006, 3),
	('PS-4471', 'tuyet.trinhthianh', NULL, 38426, 3, 36142, 'FT 2 Sprint 53', '2020-04-16 00:00:00', 10011, 3),
	('PS-4473', 'phuong.vulan', NULL, 38433, 0, 36142, 'FT 2 Sprint 53', NULL, 10011, 0),
	('PS-4478', 'phuong.vulan', NULL, 38454, 3, 37289, 'FT 2 Sprint 53', '2020-04-16 00:00:00', 10006, 3),
	('PS-4479', 'vinh.nguyenngoc', NULL, 38455, 1.5, 37041, 'FT 2 Sprint 53', '2020-04-16 00:00:00', 10011, 0),
	('PS-4484', 'phuong.vulan', NULL, 38500, 1.125, 37041, 'FT 2 Sprint 53', '2020-04-14 00:00:00', 10006, 1),
	('PS-4485', 'nv.diep', NULL, 38510, 3, 36993, 'FT 2 Sprint 53', '2020-04-17 00:00:00', 10006, 3),
	('PS-4488', 'kimanh', NULL, 38534, 3, 37289, 'FT 2 Sprint 53', '2020-04-17 00:00:00', 10006, 3),
	('PS-4489', 'vinh.nguyenngoc', NULL, 38535, 3, 36053, 'FT 2 Sprint 53', '2020-04-17 00:00:00', 10011, 0),
	('PS-4490', 'tuyet.trinhthianh', NULL, 38536, 0.1875, 36142, 'FT 2 Sprint 53', '2020-04-17 00:00:00', 10011, 0),
	('PS-4491', 'phuong.vulan', NULL, 38542, 3, 37289, 'FT 2 Sprint 53', '2020-04-17 00:00:00', 10006, 3),
	('PS-4505', 'tuyet.trinhthianh', NULL, 38602, 3, 37790, 'FT 2 Sprint 53', '2020-04-20 00:00:00', 10011, 3),
	('PS-4512', 'vinh.nguyenngoc', NULL, 38625, 3, 36053, 'FT 2 Sprint 53', '2020-04-20 00:00:00', 10011, 0),
	('PS-4515', 'kimanh', NULL, 38631, 3, 37289, 'FT 2 Sprint 53', '2020-04-20 00:00:00', 10006, 3),
	('PS-4524', 'kimanh', NULL, 38666, 3, 37289, 'FT 2 Sprint 53', '2020-04-21 00:00:00', 10006, 3),
	('PS-4526', 'tuyet.trinhthianh', NULL, 38671, 3, 37790, 'FT 2 Sprint 53', '2020-04-21 00:00:00', 10011, 3),
	('PS-4528', 'phuong.vulan', NULL, 38691, 3, 38111, 'FT 2 Sprint 53', '2020-04-20 00:00:00', 10006, 3),
	('PS-4531', 'phuong.vulan', NULL, 38725, 3, 36142, 'FT 2 Sprint 53', '2020-04-21 00:00:00', 10011, 3),
	('PS-4532', 'kimanh', NULL, 38742, 2.0625, 37289, 'FT 2 Sprint 53', '2020-04-22 00:00:00', 10006, 2),
	('PS-4533', 'tuyet.trinhthianh', NULL, 38749, 3, 37790, 'FT 2 Sprint 53', '2020-04-23 00:00:00', 10011, 0),
	('PS-4536', 'kimanh', NULL, 38786, 0.375, 37847, 'FT 2 Sprint 53', '2020-04-22 00:00:00', 10006, 0),
	('PS-4537', 'nv.diep', NULL, 38788, 2.25, 37639, 'FT 2 Sprint 53', '2020-04-22 00:00:00', 10011, 0),
	('PS-4538', 'tuyet.trinhthianh', NULL, 38790, 3, 36142, 'FT 2 Sprint 53', '2020-04-22 00:00:00', 10011, 3),
	('PS-4540', 'vinh.nguyenngoc', NULL, 38795, 3, 37041, 'FT 2 Sprint 53', '2020-04-22 00:00:00', 10011, 0),
	('PS-4541', 'phuong.vulan', NULL, 38797, 3, 37289, 'FT 2 Sprint 53', '2020-04-28 00:00:00', 10006, 0),
	('PS-4543', 'phuong.vulan', NULL, 38818, 3, 37847, 'FT 2 Sprint 53', '2020-04-22 00:00:00', 10011, 3),
	('PS-4550', 'kimanh', NULL, 38837, 0.9375, 38111, 'FT 2 Sprint 53', NULL, 10011, 0),
	('PS-4551', 'nv.diep', NULL, 38838, 3, 36993, 'FT 2 Sprint 53', '2020-04-23 00:00:00', 10006, 3),
	('PS-4552', 'phuong.vulan', NULL, 38854, 3, 37041, 'FT 2 Sprint 53', '2020-04-23 00:00:00', 10006, 3),
	('PS-4558', 'kimanh', NULL, 38911, 0.1875, 37847, 'FT 2 Sprint 53', '2020-04-23 00:00:00', 10011, 0),
	('PS-4560', 'kimanh', NULL, 38917, 1.5, 36993, 'FT 2 Sprint 53', '2020-04-24 00:00:00', 10006, 1),
	('PS-4563', 'tuyet.trinhthianh', NULL, 38924, 3, 36142, 'FT 2 Sprint 53', '2020-04-24 00:00:00', 10011, 3),
	('PS-4566', 'nv.diep', NULL, 38954, 2.25, 37639, 'FT 2 Sprint 53', '2020-04-24 00:00:00', 10006, 2),
	('PS-4577', 'kimanh', NULL, 39034, 1.5, 37041, 'FT 2 Sprint 53', '2020-04-27 00:00:00', 10006, 1),
	('PS-4578', 'tuyet.trinhthianh', NULL, 39040, 1.5, 36142, 'FT 2 Sprint 53', '2020-04-27 00:00:00', 10006, 1),
	('PS-4580', 'nv.diep', NULL, 39047, 2.25, 37639, 'FT 2 Sprint 53', '2020-04-27 00:00:00', 10006, 2),
	('PS-4591', 'phuong.vulan', NULL, 39077, 0, 37289, 'FT 2 Sprint 53', NULL, 10011, 0),
	('PS-4592', 'phuong.vulan', NULL, 39078, 0, 37041, 'FT 2 Sprint 53', NULL, 10011, 0),
	('PS-4606', 'phuong.vulan', NULL, 39150, 0, 37928, 'FT 2 Sprint 53', NULL, 10011, 0),
	('PS-4607', 'phuong.vulan', NULL, 39151, 0, 38111, 'FT 2 Sprint 53', NULL, 10011, 0),
	('PS-4608', 'phuong.vulan', NULL, 39152, 0, 38108, 'FT 2 Sprint 53', NULL, 10011, 0),
	('PS-4621', 'kimanh', NULL, 39182, 1.5, 37289, 'FT 2 Sprint 53', '2020-04-28 00:00:00', 10006, 1),
	('QA-6511', 'phuong.vulan', NULL, 38180, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6512', 'phuong.vulan', NULL, 38181, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6513', 'phuong.vulan', NULL, 38197, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6570', 'phuong.vulan', NULL, 38287, 0, 0, 'FT 2 Sprint 53', NULL, 11115, 0),
	('QA-6584', 'phuong.vulan', NULL, 38301, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6605', 'phuong.vulan', NULL, 38374, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6632', 'phuong.vulan', NULL, 38465, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6633', 'phuong.vulan', NULL, 38469, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6643', 'phuong.vulan', NULL, 38493, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6671', 'phuong.vulan', NULL, 38657, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6680', 'phuong.vulan', NULL, 38705, 0.375, 0, 'FT 2 Sprint 53', NULL, 10006, 0),
	('QA-6707', 'phuong.vulan', '2020-04-21 17:37:00', 38748, 3, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6770', 'phuong.vulan', NULL, 38898, 0, 0, 'FT 2 Sprint 53', NULL, 10006, 0),
	('QA-6775', 'phuong.vulan', NULL, 38920, 0, 0, 'FT 2 Sprint 53', NULL, 10006, 0),
	('QA-6777', 'phuong.vulan', NULL, 38926, 0, 0, 'FT 2 Sprint 53', NULL, 10006, 0),
	('QA-6784', 'phuong.vulan', NULL, 38959, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6793', 'phuong.vulan', NULL, 39003, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6828', 'vinh.nguyenngoc', NULL, 39084, 0, 0, 'FT 2 Sprint 53', NULL, 1, 0),
	('QA-6833', 'vinh.nguyenngoc', NULL, 39111, 0, 0, 'FT 2 Sprint 53', NULL, 10009, 0),
	('QA-6849', 'phuong.vulan', NULL, 39148, 3, 0, 'FT 2 Sprint 53', NULL, 1, 0);
/*!40000 ALTER TABLE `issues` ENABLE KEYS */;

-- Dumping data for table users_database.reports: ~0 rows (approximately)
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` (`report_id`, `owner`, `sprint`, `velocity`) VALUES
	(0, 1, 'FT 2 Sprint 53', 300),
	(1, 1, 'FT 2 Sprint 53', 100);
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;

-- Dumping data for table users_database.report_assignee: ~0 rows (approximately)
/*!40000 ALTER TABLE `report_assignee` DISABLE KEYS */;
INSERT INTO `report_assignee` (`id`, `assignee`, `report_id`, `velocity`) VALUES
	(0, 'nv.diep', 1, 50),
	(1, 'kimanh', 1, 50),
	(2, 'vinh.nguyenngoc', 1, 50),
	(3, 'tuyet.trinhthianh', 1, 50),
	(4, 'phuong.vulan', 1, 50);
/*!40000 ALTER TABLE `report_assignee` ENABLE KEYS */;

-- Dumping data for table users_database.report_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `report_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_details` ENABLE KEYS */;

-- Dumping data for table users_database.roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`) VALUES
	(1, 'ROLE_USER'),
	(2, 'ROLE_ADMIN');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

