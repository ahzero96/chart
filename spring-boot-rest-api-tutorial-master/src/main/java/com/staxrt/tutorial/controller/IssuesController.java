package com.staxrt.tutorial.controller;

import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.staxrt.tutorial.dto.Chart;
import com.staxrt.tutorial.exception.ResourceNotFoundException;
import com.staxrt.tutorial.model.Issues;
import com.staxrt.tutorial.model.User;
import com.staxrt.tutorial.repository.IssuesRepository;
import com.staxrt.tutorial.service.IssuesService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class IssuesController {
	
	@Autowired
    IssuesService issuesService;
	
    @Autowired
    IssuesRepository issuesRepository;

    @GetMapping("/issues")
    public List<Issues> listIssues() {
        return issuesRepository.findAll();
    }
    
    @GetMapping("/iss")
    public List<Chart> listIss() {
        return issuesService.listIss();
    }
    
    @GetMapping("/allproject")
    public List<String> allProject() {
        return issuesRepository.allProject();
    }
    
    @GetMapping("/allproject/{projectid}")
    public List<String> allSprint(@PathVariable(value = "projectid") String projectId) {
    	projectId = projectId + "%";
        return issuesRepository.sprintProject(projectId);
    }
    
    @GetMapping("/allproject/firstdate/{sprintid}")
    public List<Date> firstDate(@PathVariable(value = "sprintid") String sprintId) {
        return issuesRepository.firstDate(sprintId);
    }
    
    @GetMapping("/allproject/enddate/{sprintid}")
    public List<Date> endDate(@PathVariable(value = "sprintid") String sprintId) {
        return issuesRepository.endDate(sprintId);
    }
}
