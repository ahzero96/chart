package com.staxrt.tutorial.controller;

import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.staxrt.tutorial.dto.Chart;
import com.staxrt.tutorial.exception.ResourceNotFoundException;
import com.staxrt.tutorial.model.ERole;
import com.staxrt.tutorial.model.Issues;
import com.staxrt.tutorial.model.ReportAssignee;
import com.staxrt.tutorial.model.ReportDetails;
import com.staxrt.tutorial.model.Reports;
import com.staxrt.tutorial.model.Role;
import com.staxrt.tutorial.model.User;
import com.staxrt.tutorial.payload.MessageResponse;
import com.staxrt.tutorial.payload.SignupRequest;
import com.staxrt.tutorial.repository.IssuesRepository;
import com.staxrt.tutorial.repository.ReportAssigneeRepository;
import com.staxrt.tutorial.repository.ReportDetailsRepository;
import com.staxrt.tutorial.repository.ReportsRepository;
import com.staxrt.tutorial.service.IssuesService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class ReportsController {
	
    @Autowired
    ReportsRepository reportsRepository;
    
    @Autowired
    ReportAssigneeRepository reportAssigneeRepository;
    
    @Autowired
    ReportDetailsRepository reportDetailsRepository;

    @PostMapping("/newreport/{id}")
    public ResponseEntity<?> registerReport(@PathVariable(value = "id") int userId, @Valid @RequestBody String sprint) {
    	Reports report = new Reports();
    	report.setOnwer(userId);
    	report.setSprint(sprint);
    	report.setVelocity(100);
    	reportsRepository.save(report);
    	int reportId = reportsRepository.newestReport();
    	List<String> assignee = reportAssigneeRepository.allAssignee(sprint);
    	List<ReportDetails> details = new ArrayList<ReportDetails>();
    	for(String person : assignee) {
    		ReportAssignee reAssginee = new ReportAssignee();
    		reAssginee.setAssignee(person);
    		reAssginee.setReportId(reportId);
    		reAssginee.setVelocity(50);
    		reportAssigneeRepository.save(reAssginee);
    		int asId = reportAssigneeRepository.newestAssignee();
    		List<Date> date = reportDetailsRepository.takeDate(person);
    		float[] original = reportDetailsRepository.takeOriginal(person);
    		float[] time = reportDetailsRepository.takeTime(person);
    		for(int i = 0; i<date.size(); i++) {
    			ReportDetails inside = new ReportDetails();
    			inside.setAssigneeId(asId);
    			inside.setDate(date.get(i));
    			inside.setOriginalEstimate(original[i]);
    			inside.setTimeSpent(time[i]);
    			details.add(inside);
    			reportDetailsRepository.save(inside);
    		}
    	}
        return ResponseEntity.ok(details);
    }
    
    @GetMapping("/report/{id}")
    public ResponseEntity<List<Chart>> teamChart(@PathVariable(value = "id") int reportId) {
    	List<Object[]> allTeam= reportDetailsRepository.chartForAll(reportId);
    	List<Chart> chart = new ArrayList<Chart>();
    	allTeam.stream().forEach((record) -> {
    		Chart allIn = new Chart();
    		allIn.setDate((String) record[0]);
    		allIn.setOriginalEstimate((Double) record[1]);
    		allIn.setTimeSpent((Double) record[2]);
    		chart.add(allIn);
    });	
    	return ResponseEntity.ok(chart);
    }
    
    @GetMapping("/report/{id}/{assgineeId}")
    public ResponseEntity<List<Chart>> chartEach(@PathVariable(value = "id") int reportId,@PathVariable(value = "assgineeId") int assgineeId) {
    	List<Object[]> allTeam= reportDetailsRepository.chartForAll(reportId,assgineeId);
    	List<Chart> chart = new ArrayList<Chart>();
    	allTeam.stream().forEach((record) -> {
    		Chart allIn = new Chart();
    		allIn.setDate((String) record[0]);
    		allIn.setOriginalEstimate((Double) record[1]);
    		allIn.setTimeSpent((Double) record[2]);
    		chart.add(allIn);
    });	
    	return ResponseEntity.ok(chart);
    }
    
    @GetMapping("/report")
    public ResponseEntity<List<Reports>> listReport() {	
    	return ResponseEntity.ok(reportsRepository.findAll());
    }
    
    @GetMapping("/assignee/{id}")
    public ResponseEntity<List<ReportAssignee>> listAssingee(@PathVariable(value = "id") int reportId) {
    	return ResponseEntity.ok(reportAssigneeRepository.findByReportId(reportId));
    }
}
