package com.staxrt.tutorial.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.staxrt.tutorial.model.Issues;

public interface IssuesRepository extends JpaRepository<Issues, Long> {
	
	@Query(value = "SELECT DATE_FORMAT(users_database.issues.start_date, \"%M %d %Y\") AS start_date\r\n" + 
			"FROM users_database.issues \r\n" + 
			"where users_database.issues.start_date is not NULL \r\n" + 
			"and not (users_database.issues.issue_id = any (SELECT parent_id FROM users_database.issues)) \r\n" + 
			"and users_database.issues.start_date >= '2020-04-13 00:00:00'and users_database.issues.start_date <= '2020-04-28 00:00:00'\r\n" + 
			"and users_database.issues.sprint like 'FT 2 Sprint 53'\r\n" + 
			"GROUP BY users_database.issues.start_date\r\n" + 
			"ORDER BY users_database.issues.start_date ASC;", nativeQuery = true)
	List<String> takeDate();

	@Query(value = "SELECT Sum(users_database.issues.original_estimate) AS original_estimate\r\n" + 
			"FROM users_database.issues \r\n" + 
			"where users_database.issues.start_date is not NULL \r\n" + 
			"and not (users_database.issues.issue_id = any (SELECT parent_id FROM users_database.issues)) \r\n" + 
			"and users_database.issues.start_date >= '2020-04-13 00:00:00'and users_database.issues.start_date <= '2020-04-28 00:00:00'\r\n" + 
			"and users_database.issues.sprint like 'FT 2 Sprint 53'\r\n" + 
			"GROUP BY users_database.issues.start_date\r\n" + 
			"ORDER BY users_database.issues.start_date ASC;", nativeQuery = true)
	Double[] takeOriginal();
	
	@Query(value = "SELECT Sum(users_database.issues.time_spent) AS time_spent\r\n" + 
			"FROM users_database.issues \r\n" + 
			"where users_database.issues.start_date is not NULL \r\n" + 
			"and not (users_database.issues.issue_id = any (SELECT parent_id FROM users_database.issues)) \r\n" + 
			"and users_database.issues.start_date >= '2020-04-13 00:00:00'and users_database.issues.start_date <= '2020-04-28 00:00:00'\r\n" + 
			"and users_database.issues.sprint like 'FT 2 Sprint 53'\r\n" + 
			"GROUP BY users_database.issues.start_date\r\n" + 
			"ORDER BY users_database.issues.start_date ASC;", nativeQuery = true)
	Double[] takeTime();
	
	@Query(value = "SELECT DISTINCT LEFT(users_database.issues.sprint, 4) AS project FROM users_database.issues;", nativeQuery = true)
	List<String> allProject();
	
	@Query(value = "SELECT DISTINCT users_database.issues.sprint AS sprint FROM users_database.issues WHERE users_database.issues.sprint LIKE ?;", nativeQuery = true)
	List<String> sprintProject(String id);
	
	@Query(value = "SELECT DISTINCT MIN(users_database.issues.start_date) AS minday FROM users_database.issues WHERE users_database.issues.sprint LIKE ?;", nativeQuery = true)
	List<Date> firstDate(String id);
	
	@Query(value = "SELECT DISTINCT MAX(users_database.issues.start_date) AS maxday FROM users_database.issues WHERE users_database.issues.sprint LIKE ?;", nativeQuery = true)
	List<Date> endDate(String id);
}
