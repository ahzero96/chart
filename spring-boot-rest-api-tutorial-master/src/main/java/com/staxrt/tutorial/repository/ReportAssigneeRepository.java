package com.staxrt.tutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.staxrt.tutorial.model.ReportAssignee;
import com.staxrt.tutorial.model.Reports;

import java.util.List;
import java.util.Optional;

public interface ReportAssigneeRepository extends JpaRepository<ReportAssignee, Long> {
	
	@Query(value = "SELECT DISTINCT assignee FROM issues WHERE assignee NOT LIKE 0 AND sprint LIKE ?;", nativeQuery = true)
	List<String> allAssignee(String spirnt);
	
	@Query(value = "SELECT MAX(id) FROM report_assignee;", nativeQuery = true)
	int newestAssignee();

	List<ReportAssignee> findByReportId(int reportId);
}
