package com.staxrt.tutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.staxrt.tutorial.model.Reports;

import java.util.Optional;

public interface ReportsRepository extends JpaRepository<Reports, Long> {
	
	@Query(value = "SELECT MAX(report_id) FROM reports;", nativeQuery = true)
	int newestReport();

}
