package com.staxrt.tutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.staxrt.tutorial.model.ERole;
import com.staxrt.tutorial.model.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
