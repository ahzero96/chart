package com.staxrt.tutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.staxrt.tutorial.model.ReportDetails;
import com.staxrt.tutorial.model.Reports;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ReportDetailsRepository extends JpaRepository<ReportDetails, Long> {

	@Query(value = "SELECT users_database.issues.start_date\r\n" + 
			"FROM users_database.issues \r\n" + 
			"where users_database.issues.start_date is not NULL \r\n" + 
			"and not (users_database.issues.issue_id = any (SELECT parent_id FROM users_database.issues)) \r\n" + 
			"and users_database.issues.start_date >= '2020-04-13 00:00:00'and users_database.issues.start_date <= '2020-04-28 00:00:00'\r\n" + 
			"and users_database.issues.sprint like 'FT 2 Sprint 53'\r\n" + 
			"and users_database.issues.assignee like ?\r\n" + 
			"GROUP BY users_database.issues.start_date\r\n" + 
			"ORDER BY users_database.issues.start_date ASC;", nativeQuery = true)
	List<Date> takeDate(String assignee);

	@Query(value = "SELECT Sum(users_database.issues.original_estimate) AS original_estimate\r\n" + 
			"FROM users_database.issues \r\n" + 
			"where users_database.issues.start_date is not NULL \r\n" + 
			"and not (users_database.issues.issue_id = any (SELECT parent_id FROM users_database.issues)) \r\n" + 
			"and users_database.issues.start_date >= '2020-04-13 00:00:00'and users_database.issues.start_date <= '2020-04-28 00:00:00'\r\n" + 
			"and users_database.issues.sprint like 'FT 2 Sprint 53'\r\n" + 
			"and users_database.issues.assignee like ?\r\n" + 
			"GROUP BY users_database.issues.start_date\r\n" + 
			"ORDER BY users_database.issues.start_date ASC;", nativeQuery = true)
	float[] takeOriginal(String assignee);
	
	@Query(value = "SELECT Sum(users_database.issues.time_spent) AS time_spent\r\n" + 
			"FROM users_database.issues \r\n" + 
			"where users_database.issues.start_date is not NULL \r\n" + 
			"and not (users_database.issues.issue_id = any (SELECT parent_id FROM users_database.issues)) \r\n" + 
			"and users_database.issues.start_date >= '2020-04-13 00:00:00'and users_database.issues.start_date <= '2020-04-28 00:00:00'\r\n" + 
			"and users_database.issues.sprint like 'FT 2 Sprint 53'\r\n" + 
			"and users_database.issues.assignee like ?\r\n" + 
			"GROUP BY users_database.issues.start_date\r\n" + 
			"ORDER BY users_database.issues.start_date ASC;", nativeQuery = true)
	float[] takeTime(String assignee);
	
	@Query(value = "SELECT DATE_FORMAT(report_details.date,'%M %d %Y'), SUM(report_details.original_estimate), SUM(report_details.time_spent) FROM report_details\r\n" + 
			"WHERE report_details.assignee_id = ANY (SELECT report_assignee.id FROM report_assignee WHERE report_assignee.report_id = ?)\r\n" + 
			"GROUP BY report_details.date\r\n" + 
			"ORDER BY report_details.date ASC", nativeQuery = true)
	List<Object[]> chartForAll(int reportId);
	
	@Query(value = "SELECT DATE_FORMAT(report_details.date,'%M %d %Y'), SUM(report_details.original_estimate), SUM(report_details.time_spent) FROM report_details\r\n" + 
			"WHERE report_details.assignee_id = ANY (SELECT report_assignee.id FROM report_assignee WHERE report_assignee.report_id = ?)\r\n" + 
			"AND report_details.assignee_id = ?\r\n" + 
			"GROUP BY report_details.date\r\n" + 
			"ORDER BY report_details.date ASC", nativeQuery = true)
	List<Object[]> chartForAll(int reportId, int assigneeId);
}
