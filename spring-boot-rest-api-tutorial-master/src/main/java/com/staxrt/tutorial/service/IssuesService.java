package com.staxrt.tutorial.service;

import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.staxrt.tutorial.dto.Chart;
import com.staxrt.tutorial.exception.ResourceNotFoundException;
import com.staxrt.tutorial.model.Issues;

import com.staxrt.tutorial.repository.IssuesRepository;

@Configuration
public class IssuesService{
	
    @Autowired
    IssuesRepository issuesRepository;

    public List<Chart> listIss() {
   	List<Chart> install = new ArrayList<>();
   	List<String> date = issuesRepository.takeDate();
   	Double[] original = issuesRepository.takeOriginal();
   	Double[] time= issuesRepository.takeTime();
   	for(int i = 0; i< date.size(); i++) {
   		Chart e = new Chart();
   		e.setDate(date.get(i));
   		e.setOriginalEstimate(original[i]);
   		e.setTimeSpent(time[i]);
   		install.add(e);
   	}
        return install;
    }
}
