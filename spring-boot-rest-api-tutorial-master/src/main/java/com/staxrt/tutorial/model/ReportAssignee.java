package com.staxrt.tutorial.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(	name = "report_assignee")
public class ReportAssignee {
	
	@Id
	@Column(name ="id",nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
		
	@Column(name ="report_id",nullable = false)
	private int reportId;
	
	@Column(name ="assignee",nullable = false)
	private String assignee;
	
	@Column(name ="velocity",nullable = false)
	private float velocity;
	
	public ReportAssignee() {
    }
	
	public ReportAssignee(int id, int reportId, String assignee, float velocity) {
		this.id = id;
		this.reportId = reportId;
		this.assignee = assignee;
		this.velocity = velocity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public float getVelocity() {
		return velocity;
	}

	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}


	@Override
	public String toString() {
		return "ReportAssignee [id=" + id + ", reportId=" + reportId + ", assignee=" + assignee + ", velocity="
				+ velocity + ", reportDetails=" + "]";
	}
	
}
