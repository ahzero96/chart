package com.staxrt.tutorial.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(	name = "report_details")
public class ReportDetails {
	
	@Id
	@Column(name ="id",nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name ="assignee_id",nullable = false)
	private int assigneeId;
		
	@Column(name ="original_estimate",nullable = false)
	private float originalEstimate;
	
	@Column(name ="time_spent",nullable = false)
	private float timeSpent;
	
	@Column(name ="date",nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	public ReportDetails() {
    }

	public ReportDetails(int id, int assigneeId, float originalEstimate, float timeSpent, Date date) {
		super();
		this.id = id;
		this.assigneeId = assigneeId;
		this.originalEstimate = originalEstimate;
		this.timeSpent = timeSpent;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(int assigneeId) {
		this.assigneeId = assigneeId;
	}

	public float getOriginalEstimate() {
		return originalEstimate;
	}

	public void setOriginalEstimate(float originalEstimate) {
		this.originalEstimate = originalEstimate;
	}

	public float getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(float timeSpent) {
		this.timeSpent = timeSpent;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ReportDetails [id=" + id + ", assigneeId=" + assigneeId + ", originalEstimate=" + originalEstimate
				+ ", timeSpent=" + timeSpent + ", date=" + date + "]";
	}
	

	
}
