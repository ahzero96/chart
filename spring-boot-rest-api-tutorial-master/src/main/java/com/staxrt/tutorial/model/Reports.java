package com.staxrt.tutorial.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(	name = "reports")
public class Reports {
	
	@Id	
	@Column(name ="report_id",nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int reportId;
	
	@Column(name ="sprint",nullable = false)
	private String sprint;
	
	@Column(name ="velocity",nullable = false)
	private float velocity;
	
	@Column(name ="owner",nullable = false)
	private int onwer;

	public Reports() {
	}
	
	public Reports(int reportId, String sprint, float velocity, int onwer) {
		super();
		this.reportId = reportId;
		this.sprint = sprint;
		this.velocity = velocity;
		this.onwer = onwer;
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public String getSprint() {
		return sprint;
	}

	public void setSprint(String sprint) {
		this.sprint = sprint;
	}

	public float getVelocity() {
		return velocity;
	}

	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}

	public int getOnwer() {
		return onwer;
	}

	public void setOnwer(int onwer) {
		this.onwer = onwer;
	}

	@Override
	public String toString() {
		return "Reports [reportId=" + reportId + ", sprint=" + sprint + ", velocity=" + velocity + ", onwer=" + onwer
				+ ", reportAssignee=" + "]";
	}
	
}
