package com.staxrt.tutorial.model;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
