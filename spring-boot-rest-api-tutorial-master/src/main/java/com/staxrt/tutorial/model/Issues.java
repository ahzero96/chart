package com.staxrt.tutorial.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.micrometer.core.lang.Nullable;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(	name = "issues")
public class Issues {
	
	@Id
	@Column(name ="issue_key")
	private String issueKey;
	
	@Column(name ="issue_id",nullable = false)
	private int issueId;
	
	@Column(name ="parent_id",nullable = true)
	private int parentId;
	
	@Column(name ="original_estimate",nullable = false)
	private float originalEstimate;
	
	@Column(name ="assignee",nullable = false)
	private String assignee;
	
	@Column(name ="status",nullable = false)
	private int status;
	
	@Column(name ="start_date",nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Column(name ="end_date",nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	@Column(name ="sprint",nullable = false)
	private String sprint;
	
	@Column(name ="time_spent",nullable = true)
	private float timeSpent;
	
	public Issues() {
    }
	
	public Issues(String issueKey, int issueId, int parentId, float originalEstimate, String assignee, 
			int status, Date startDate, Date endDate, String sprint, float timeSpent) {
		this.issueKey = issueKey;
		this.issueId = issueId;
		this.parentId = parentId;
		this.originalEstimate = originalEstimate;
		this.assignee = assignee;
		this.status = status;
		this.startDate = startDate;
		this.endDate = endDate;
		this.sprint = sprint;
		this.timeSpent = timeSpent;
	}

	public String getIssueKey() {
		return issueKey;
	}

	public void setIssueKey(String issueKey) {
		this.issueKey = issueKey;
	}

	public int getIssueId() {
		return issueId;
	}

	public void setIssueId(int issueId) {
		this.issueId = issueId;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public float getOriginalEstimate() {
		return originalEstimate;
	}

	public void setOriginalEstimate(float originalEstimate) {
		this.originalEstimate = originalEstimate;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getSprint() {
		return sprint;
	}

	public void setSprint(String sprint) {
		this.sprint = sprint;
	}

	public float getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(float timeSpent) {
		this.timeSpent = timeSpent;
	}

	@Override
	public String toString() {
		return "Issue [issueKey=" + issueKey + ", issueId=" + issueId + ", parentId=" + parentId + ", originalEstimate="
				+ originalEstimate + ", assignee=" + assignee + ", status=" + status + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", sprint=" + sprint + ", timeSpent=" + timeSpent + "]";
	}

}
