package com.staxrt.tutorial.dto;

import java.util.Date;

public class Chart {

	private String date;
	
	private Double originalEstimate;
	
	private Double timeSpent;
	
	public Chart() {
	}
	
	public Chart(String date, Double originalEstimate, Double timeSpent) {
		this.date = date;
		this.originalEstimate = originalEstimate;
		this.timeSpent = timeSpent;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Double getOriginalEstimate() {
		return originalEstimate;
	}

	public void setOriginalEstimate(Double originalEstimate) {
		this.originalEstimate = originalEstimate;
	}

	public Double getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(Double timeSpent) {
		this.timeSpent = timeSpent;
	}

	@Override
	public String toString() {
		return "Chart [date=" + date + ", originalEstimate=" + originalEstimate + ", timeSpent=" + timeSpent + "]";
	}
	
}
