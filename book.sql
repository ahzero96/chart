﻿INSERT INTO `author` (`author_id`, `author_name`, `author_infor`) VALUES
	(1, 'Ngô Tất Tố', 'Ngô Tất Tố sinh năm 1894 ở làng Lộc Hà, tổng Hội Phụ, phủ Từ Sơn, Bắc Ninh (nay là thôn Lộc Hà, xã Mai Lâm, huyện Đông Anh, Hà Nội). Ông là con thứ hai, nhưng là trưởng nam trong một gia đình có bảy anh chị em, ba trai, bốn gái. Lúc còn nhỏ Ngô Tất Tố được thụ hưởng một nền dục Nho học. Từ năm 1898, Ngô Tất Tố được ông nội dạy vỡ lòng chữ Hán ở quê, sau đó ông theo học ở nhiều làng quê trong vùng. Năm 1912, Ngô Tất Tố học tư chữ Pháp một thời gian ngắn và bắt đầu tham dự các kỳ thi truyền thống lúc bấy giờ vẫn còn được triều đình nhà Nguyễn tổ chức. Ông đỗ kỳ sát hạch, nhưng thi hương bị hỏng ở kỳ đệ nhất. Đến năm 1915, ông đỗ đầu kỳ khảo hạch toàn tỉnh Bắc Ninh, nên được gọi là đầu xứ Tố, rồi thi hương lần thứ hai, khoa Ất Mão, cũng là khoa thi hương cuối cùng ở Bắc Kì. Ông qua được kỳ đệ nhất, nhưng bị hỏng ở kỳ đệ nhị  Năm 1926, Ngô Tất Tố ra Hà Nội làm báo. Ông viết cho tờ An Nam tạp chí. Nhưng vì thiếu tiền, tờ báo này phải tự đình bản, Ngô Tất Tố cùng với Tản Đà đã vào Sài Gòn. Mặc dù không thật sự thành công trong cuộc thử sức ở Nam Kì, nhưng tại đây, Ngô Tất Tố đã có cơ hội tiếp cận với tri thức và văn hóa thế giới ở vùng đất khi đó là thuộc địa chính thức của Pháp cũng như theo đuổi nghề báo để chuẩn bị sau này trở thành một nhà báo chuyên nghiệp. Trong thời kỳ này, ông viết với các bút danh Bắc Hà, Thiết Khẩu Nhi, Lộc Hà, Tân Thôn Dân...  Sau gần ba năm ở Sài Gòn, Ngô Tất Tố trở ra Hà Nội. Ông tiếp tục sinh sống bằng cách viết bài cho các báo: An Nam tạp chí, Thần chung, Phổ thông, Đông Dương, Hải Phòng tuần báo, Thực nghiệp, Con ong, Việt nữ, Tiểu thuyết thứ ba, Tương lai, Công dân, Đông Pháp thời báo, Thời vụ, Hà Nội tân văn,Tuần lễ... với 29 bút danh khác nhau như: Thục Điểu, Lộc Hà, Lộc Đình, Thôn Dân, Phó Chi, Tuệ Nhơn, Thuyết Hải, Xuân Trào, Hy Cừ... Trong thời gian những năm 1936-1939, Ngô Tất Tố viết nhiều tác phẩm chỉ trích quan lại tham nhũng phong kiến.  Hà Văn Đức, trong bài viết Ngô Tất Tố - Nhà văn tin cậy của nông dân (báo Nhân dân, ngày 10 tháng 6 năm 1997), cho biết năm 1935, Ngô Tất Tố từng bị chánh sở mật thám Hà Nội gọi lên '),
	(2, 'Nam Cao', 'ok baby'),
	(3, 'Fujiko Fujio', 'ccc'),
	(4, 'Nguyễn Nhật Ánh', 'bbbb'),
	(5, 'Dale Carnegie', 'aaa'),
	(6, 'Eran Katz ', 'aaa'),
	(7, 'Aoyama', 'aaa'),
	(8, 'Donald E. Knuth', 'bbbb'),
	(10, 'Allen B. Downey', 'bbbb'),
	(11, 'Peterson Silgerschatz', 'as'),
	(12, 'Stephen King', 'a'),
	(13, 'Tsugumi Ohba', 'a'),
	(14, 'Reki Kawahara', 'a');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;

-- Dumping data for table baitap.book: ~16 rows (approximately)
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` (`book_id`, `author_id`, `book_name`, `status`, `stock`, `create_date`) VALUES
	(1, 6, 'Trí tuệ Do Thái', 1, 15, '2020-05-28'),
	(2, 10, 'Think Python 2E', 2, 9, '2020-05-28'),
	(3, 8, 'Data Science', 2, 8, '2020-05-28'),
	(4, 1, 'Tắt Đèn', 1, 5, '2020-05-28'),
	(5, 14, 'Sword Art Online', 1, 0, '2020-06-14'),
	(6, 12, 'The Dark Tower', 2, 0, '2020-06-04'),
	(7, 11, 'Operating System Concepts', 4, 8, '2020-06-04'),
	(8, 7, 'Thám tử lừng danh Conan', 3, 20, '2020-05-28'),
	(9, 2, 'Sống mòn', 1, 8, '2020-05-30'),
	(10, 3, 'Doraemon', 1, 8, '2020-05-28'),
	(11, 2, 'Lão Hạc', 1, 9, '2020-05-28'),
	(12, 13, 'Death Note', 1, 5, '2020-06-14'),
	(13, 5, 'Đắc Nhân Tâm', 2, 13, '2020-05-28'),
	(14, 12, 'It', 4, NULL, '2020-06-15'),
	(15, 8, 'Data Analytics', 3, 8, '2020-05-28'),
	(16, 4, 'Mắt Biếc', 2, 10, '2020-05-28'),
	(17, 4, 'Tôi thấy hoa vàng trên cỏ xanh', 1, 2, '2020-05-30');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;

-- Dumping data for table baitap.buying: ~1 rows (approximately)
/*!40000 ALTER TABLE `buying` DISABLE KEYS */;
INSERT INTO `buying` (`book_name`, `author_name`) VALUES
	('Horror Collections', 'Junji Ito');
/*!40000 ALTER TABLE `buying` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
