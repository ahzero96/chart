SELECT DATE_FORMAT(users_database.issues.start_date, "%M %d %Y") AS start_date, Sum(users_database.issues.original_estimate) AS original_estimate, Sum(users_database.issues.time_spent) AS time_spent
FROM users_database.issues 
where users_database.issues.start_date is not NULL 
and not (users_database.issues.issue_id = any (SELECT parent_id FROM users_database.issues)) 
and users_database.issues.start_date >= '2020-04-13 00:00:00'and users_database.issues.start_date <= '2020-04-28 00:00:00'
and users_database.issues.sprint like 'FT 2 Sprint 53'
GROUP BY users_database.issues.start_date
ORDER BY users_database.issues.start_date ASC;

SELECT DISTINCT LEFT(users_database.issues.sprint, 4) AS project FROM users_database.issues;

SELECT DISTINCT users_database.issues.sprint AS sprint FROM users_database.issues WHERE users_database.issues.sprint LIKE 'FT 2%';

SELECT DISTINCT MIN(users_database.issues.start_date) AS minday FROM users_database.issues WHERE users_database.issues.sprint LIKE 'FT 2 Sprint 53'
UNION
SELECT DISTINCT MAX(users_database.issues.start_date) AS maxday FROM users_database.issues WHERE users_database.issues.sprint LIKE 'FT 2 Sprint 53';


SELECT Sum(users_database.issues.time_spent) AS time_spent
			FROM users_database.issues
			where users_database.issues.start_date is not NULL
			and not (users_database.issues.issue_id = any (SELECT parent_id FROM users_database.issues))
			and users_database.issues.start_date >= '2020-04-13 00:00:00'and users_database.issues.start_date <= '2020-04-28 00:00:00'
			and users_database.issues.sprint like 'FT 2 Sprint 53'
			GROUP BY users_database.issues.start_date
			ORDER BY users_database.issues.start_date ASC;
			
SELECT DATE_FORMAT(users_database.issues.start_date, "%M %d %Y") AS start_date
FROM users_database.issues 
where users_database.issues.start_date is not NULL 
and not (users_database.issues.issue_id = any (SELECT parent_id FROM users_database.issues)) 
and users_database.issues.start_date >= '2020-04-13 00:00:00'and users_database.issues.start_date <= '2020-04-28 00:00:00'
and users_database.issues.sprint like 'FT 2 Sprint 53'
GROUP BY users_database.issues.start_date
ORDER BY users_database.issues.start_date ASC;

SELECT DISTINCT assignee FROM issues WHERE assignee NOT LIKE 0 AND sprint LIKE 'FT 2 Sprint 53';

SELECT MAX(report_id) FROM reports;