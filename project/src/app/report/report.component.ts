import { Component, OnInit } from '@angular/core';
import { TestService } from '../service/test.service';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  listReport = [];
  
  constructor(private test: TestService) {
    this.test.allReport().subscribe(
      data =>{
        this.listReport = data;

      },
      err => {

      }
    );

   }
  
  ngOnInit(): void {
  }

}
