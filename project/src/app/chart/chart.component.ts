import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TestService } from '../service/test.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  assignee = [];

  constructor(private route: ActivatedRoute, private test: TestService) { 
    this.test.assingneeReport(this.route.snapshot.params.reportId).subscribe(
      data =>{
        this.assignee = data;
      },
      err =>{}
    );
   
  }

  ngOnInit(): void {
  }

}
