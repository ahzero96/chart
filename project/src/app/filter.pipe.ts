import { Pipe, PipeTransform } from '@angular/core';
import { Book } from './model/book.model'
@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(books: Book[], searchText: string): Book[] {

    if (!books || !searchText) {
      return books;
    }

    return books.filter(book => 
      book.bookName.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
  }

}
