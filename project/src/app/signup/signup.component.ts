import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { Signup } from '../model/signup.model';
import { Router } from '@angular/router';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signModel: Signup = new Signup();

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  signupUser() {
    this.authService.register(this.signModel).subscribe(data => {
      this.router.navigateByUrl('/list-user');
      console.log(data);
    }, err => {
      console.log(err);
    });
  }

}
