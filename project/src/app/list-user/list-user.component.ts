import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  listUser = []

  constructor(private userService: UserService, private router: Router, private activeRoute: ActivatedRoute) {
    this.userService.getAllUser().subscribe(data => {
      this.listUser = data;
      console.log(this.listUser);
    }, err => {

    });
    console.log('aa');
   }

  ngOnInit(): void {
  }

  deleteUser(id: number){
    this.userService.deleteUserById(id).subscribe(data => {
      this.userService.getAllUser().subscribe(data => {
        this.listUser = data;
        console.log(this.listUser);
      }, err => {
  
      });
    }, err => {
      console.log(err);
    });
  }

  changPass(){
    
  }
}
