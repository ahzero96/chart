import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Chart } from '../model/chart.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Report } from '../model/report.model';
import { ReportAssignee } from '../model/reportAssignee.model';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  baseUrl = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) { }

  getAllChart(): Observable<Chart[]> {
    return this.http.get<Chart[]>(`${this.baseUrl}/iss`);
  }

  getAllProject(): Observable<String[]> {
    return this.http.get<String[]>(`${this.baseUrl}/allproject`);
  }

  getAllSprint(sprint): Observable<String[]> {
    return this.http.get<String[]>(`${this.baseUrl}/allproject/${sprint}`);
  }
  
  getFirst(sprint): Observable<Date> {
    return this.http.get<Date>(`${this.baseUrl}/allproject/firstdate/${sprint}`);
  }
  
  getEnd(sprint): Observable<Date> {
    return this.http.get<Date>(`${this.baseUrl}/allproject/enddate/${sprint}`);
  }



  newReport(user,sprint): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/newreport/${user}`, sprint);
  }


  allReport(): Observable<Report[]> {
    return this.http.get<Report[]>(`${this.baseUrl}/report`);
  }

  assingneeReport(id): Observable<ReportAssignee[]> {
    return this.http.get<ReportAssignee[]>(`${this.baseUrl}/assignee/${id}`);
  }
}
