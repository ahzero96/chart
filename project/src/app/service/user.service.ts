import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) { }

  getAllUser(): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseUrl}/user`);
  }

  getUserById(id): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/user/${id}`);
  }

  updateUser(id, pass): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/user/${id}`, pass);
  }

  deleteUserById(id): Observable<User> {
    return this.http.delete<User>(`${this.baseUrl}/user/${id}`);
  }
}
