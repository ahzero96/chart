import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../model/book.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  baseUrl = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) { }

  getAllBook(): Observable<Book[]> {
    // return this.http.get<Employee[]>(this.baseUrl + '/employees');
    return this.http.get<Book[]>(`${this.baseUrl}/book`);
  }

  addBook(emp): Observable<Book> {
    return this.http.post<Book>(`${this.baseUrl}/book`, emp);
  }

  getBookById(id): Observable<Book> {
    return this.http.get<Book>(`${this.baseUrl}/book/${id}`);
  }
  
  updateBook(id, emp): Observable<Book> {
    return this.http.put<Book>(`${this.baseUrl}/book/${id}`, emp);
  }

  deleteBookById(id): Observable<Book> {
    return this.http.delete<Book>(`${this.baseUrl}/book/${id}`);
  }
}
