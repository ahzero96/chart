import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ListUserComponent } from './list-user/list-user.component';
import { ReportComponent } from './report/report.component';
import { ChartComponent } from './chart/chart.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'list-user', component: ListUserComponent},
  {path: 'report-list', component: ReportComponent},
  {path: 'chart/:reportId', component: ChartComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
