import { Component, OnInit } from '@angular/core';
import { Login } from '../model/login.model';

import { AuthService } from '../service/auth.service';

import { TokenStorageService } from '../service/token-storage.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginModel: Login = new Login();


  constructor(private authService: AuthService, private tokenStorage: TokenStorageService, private router: Router) { 
    
  
    
  }

  ngOnInit() {
  }


  login() {
    this.authService.login(this.loginModel).subscribe(data => {
      
      console.log(data);
      this.tokenStorage.saveToken(data.token);
      this.tokenStorage.saveUser(data);
      window.location.reload();
    }, err => {

    });
    this.router.navigateByUrl('');
  }


}
