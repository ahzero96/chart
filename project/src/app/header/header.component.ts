import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../service/token-storage.service';
import { Router } from '@angular/router';
import { TestService } from '../service/test.service';
import { Chart } from '../model/chart.model';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit{
    currentUser: any;

    chart = [];
    project = [];
    sprint = [];
    startDate: Date;
    endDate: Date;
    saveSprint: string;

    constructor(private token: TokenStorageService, private router: Router,private test: TestService){

        // Take data from Chart
    this.test.getAllChart().subscribe(data => {
      this.chart = data;
    }, err => {

    });
    // 


    this.test.getAllProject().subscribe(data => {
      this.project = data;
    }, err => {

    });
    }

    ngOnInit(): void {
        this.currentUser = this.token.getUser();

    }
    display(str: string){
      if (str.localeCompare('ROLE_USER') === 0){
        return true;
      } else {
        return false;
      }
    }

    logOut(){
        this.token.signOut();
        window.location.reload();
        this.router.navigateByUrl('/login');
    }


    
  getSprint(emp: string){
    this.test.getAllSprint(emp).subscribe(data => {
      this.sprint = data;
    }, err => {

    });

  }


  getDate(emp: string){
    this.saveSprint = emp;
    this.test.getFirst(emp).subscribe(data => {
      this.startDate = data;
    }, err => {

    });

    this.test.getEnd(emp).subscribe(data => {
      this.endDate = data;
    }, err => {

    });
  }

  saveReport(){
    this.test.newReport(this.currentUser.id,this.saveSprint).subscribe(data => {
      console.log('Done');
    }, err => {

    });

  }
}

