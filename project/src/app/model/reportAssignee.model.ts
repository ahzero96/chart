export class ReportAssignee {
    id: number;
    assignee: string;
    reportId: number;
    velocity: string;
}