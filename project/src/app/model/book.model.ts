export class Book {
    id: number;
    bookName: string;
    bookType: string;
    bookPrice: number;
    bookImage: string;
}